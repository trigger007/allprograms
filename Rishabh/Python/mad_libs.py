import os 
import sys
import random
from math import *
print("This is a game called Mad Libs")
print("Type in the details to see the magic\n")
ch=input("is your character male or female? :\n")
print()
print()

if ch=="male":
    name=input("Type in the name of your character :")
    sword=input("Type in the name of your character's sword :")
    colour=input("Type in your favorite colour :")
    villain=input("Type in the name of the villain of your story :")
    print()
    print("Once upon a time , there lived a young boy... ")
    print( "named " +name)
    print("he had a mighty sword called "+sword)
    print(name+" loved his sword")
    print("his sword was "+colour+" in colour")
    print("he and his sword had some awesome adventures")
    print("They had also killed the ferocious villain of his town... "+villain)

elif ch=="female":
    name=input("Type in the name of your character :")
    sword=input("Type in the name of your character's sword :")
    colour=input("Type in your favorite colour :")
    villain=input("Type in the name of the villain of your story :")
    print()
    print("Once upon a time , there lived a young girl... ")
    print( "named " +name)
    print("she had a mighty sword called "+sword)
    print(name+" loved her sword")
    print("her sword was "+colour+" in colour")
    print("she and her sword had some awesome adventures")
    print("They had also killed the ferocious villain of her town... "+villain)


