import java.util.ArrayList;

class MyClass {
  String name;
  int age;

  public MyClass (String n, int a) {
	this.name=n;
  	this.age=a;
  }
}

class testClass {
  public static void main(String[] args) {
    ArrayList<MyClass> myObj = new ArrayList<MyClass>();
    for (int i=0; i<10; i++) {
	    MyClass c = new MyClass ("Vishal", 45+i);
	    myObj.add(c);
    }
    for (int i=0; i<myObj.size(); i++) {
    	System.out.println(myObj.get(i).name + ":" + myObj.get(i).age);
    }

  }
}
