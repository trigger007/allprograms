import java.util.*;
public class buzz
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner (System.in);
        System.out.println("This program finds out if a number is a buzz number or not. ");
        System.out.println("A number is a buzz number if the last digit of the number is 7 and the number is divisible by 7."); 
        char l = ' ';
        do
        {
           System.out.println("Enter a number: ");
           int num = sc.nextInt();
           if (num%7 == 0 && num%10 == 7)
               System.out.println("The number is a Buzz number: ");
           else 
               System.out.println("The number is not a Buzz number: ");
           System.out.println("Do you want to try again? (y or n): ");
           l = sc.next().charAt(0);
        }
        while (l == 'y');
    }
}

        
            
