import java.util.*;
public class add_on
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the value: ");
            int a = sc.nextInt();
            int b = 0;
            for(int i=1 ; i<=a ; i++)
            {
                b = b+i;
            }
            System.out.println("The sum of all the natural numbers less than "+a+" is "+b);
            System.out.println("Do you want to try again? (y or n) ");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
