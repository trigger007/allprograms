import java.util.*;
public class menu
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char a = ' ';;
        do
        {
            System.out.println("Enter 1 for finding the value of an entered digit to the power of another entered digit: ");
	    System.out.println("Enter 2 for finding the sum of the root of two entered digits: ");
            int menu = sc.nextInt();
            if (menu == 1)
            {
                System.out.println("Enter the base value: ");
                double x = sc.nextDouble();
	        System.out.println("Enter the power value: ");
                double y = sc.nextDouble();
                double ans = (Math.pow(x,y));
                System.out.println("The value of x to the power of y is: " + ans);
            }
            else if (menu == 2)
            {
                System.out.println("Enter the first value: ");
                double x = sc.nextDouble();
                System.out.println("Enter the second value: ");
                double y = sc.nextDouble();
                double answ = ((Math.sqrt(x))+(Math.sqrt(y)));
                System.out.println("The value of the sum of root x and root y is: " + answ);
            }
            else 
            {
                System.out.println("Invalid input!");
            }
            System.out.println("Do you want to try again? (y or n): ");
            a = sc.next().charAt(0);
        }
        while (a == 'y');
    }
}

