import java.util.*;
public class digit_add
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char a = ' ';
        int b=0;
        do
        {
             System.out.println("Enter the number : ");
             long num = sc.nextLong();
             while (num > 0)
             {
                 long q = num%10;
                 b+=q;
                 num/=10;
             }
             System.out.println("The sum of all the digits of the number: " + b);
             System.out.println("Do you want to try again? (y or n): ");
             a = sc.next().charAt(0);
             b = 0;
        }
        while (a == 'y');
    }
}
