import java.util.*;
public class calculator
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char q;
        do
        {
            System.out.println("Enter the first value: ");
            double a = sc.nextDouble();
            System.out.println("Enter the second value: ");
            double b = sc.nextDouble();
            System.out.println("MENU:\nEnter '+' for Addition\nEnter '-' for Subtraction\nEnter '*' for Multiplication\nEnter '/' for Division\nEnter '%' for Modulous Division: ");
            char x = sc.next().charAt(0);
            if (x == '+')
                System.out.println("The addition of the two numbers is: " + (a+b));
            else if (x == '-')
                System.out.println("The subtraction of the two numbers is: " + (a-b));
            else if (x == '*')
                System.out.println("The multiplication of the two numbers is : " + (a*b));
            else if (x == '/')
                System.out.println("The division of the two numbers is: " + (a/b));
            else if (x == '%')
                System.out.println("The remainder between the two numbers is: " + (a%b));
            else
             System.out.println("Invalid input! ");
            System.out.println("Do you want to try again? \n (y or n)");
            q = sc.next().charAt(0);
        }
        while (q == 'y'); 
    }
}
            
