import java.util.*;
public class pattern_1
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner(System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the number of terms required in the pattern: ");
            int n = sc.nextInt();
            int a = 5;
            for (int i=1 ; i<=n ; i++)
            {
                for(int j=1 ; j<=i ; j++)
                {
                    System.out.print(a+" ");
                    a+=5;
                }
                System.out.println();
            }
            System.out.println("Do you want o try again?(y or n)");
            x=sc.next().charAt(0);
        }
        while(x=='y');
    }
}
