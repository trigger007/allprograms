import java.util.*;
public class pattern_4
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter 1 for first pattern and enter 2 for second pattern: ");
            int ent = sc.nextInt();
            if(ent == 1)
            {
                System.out.println("Enter the number of terms required in the pattern: ");
                int n = sc.nextInt();
                for(int i=1 ; i<=n ; i++)
                {
                    int col=64+i;
                    for(int j=1 ; j<=i ; j++)
                    {
                        System.out.print((char)col +" ");
                    }
                    System.out.println();
                }
            }
            else if(ent == 2)
            {
                System.out.println("Enter the number of terms required in the pattern: ");
                int no = sc.nextInt();
                int col1 = 65;
                for(int i=no ; i>=1 ; i--)
                {
                    for (int k=1 ; k<=(no-i) ; k++)
                    {
                        System.out.print(" ");
                    }
                    for(int j=1 ; j<=i ; j++)
                    { 
                        System.out.print((char)col1++ +" ");
                    }
                    System.out.println();    
                }
            }
            System.out.println("Do you want to try again?(y or n)");
            x=sc.next().charAt(0);
        }
        while(x=='y');
    }
}
