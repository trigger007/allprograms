import java.util.*;
public class pattern_6
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner (System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the number of rows for the pattern: ");
            int rows = sc.nextInt();
            System.out.println("Enter the value whose tables is to be taken for the pattern: ");
            int val = sc.nextInt();
            for(int i=rows ; i>=1 ; i--)
            {
                int valu = val;
                for(int k=1; k<=(rows-i) ; k++)
                System.out.print(" ");
                for(int j=1,q=2 ; j<=i ; j++,valu+=val)
                {
                    System.out.print(valu+" ");
                }
                System.out.println();
            }
            System.out.println("Do you want to try again?(y or n)");
            x=sc.next().charAt(0);
        }
        while(x=='y');
    }
}
