import java.util.*;
public class big_small
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
	char x = ' ';
	do
	{
            System.out.println("Enter first digit: ");
            int a = sc.nextInt();
            System.out.println("Enter second digit: "); 
            int b = sc.nextInt();
            System.out.println("Enter third digit: ");
            int c = sc.nextInt();
            if((a>=1&&a<=9)&&(b>=1&&b<=9)&&(c>=1&&c<=9))
            {
                int d = (100*a)+(10*b)+(c);
                int e = (100*b)+(10*c)+(a);
                int f = (100*c)+(10*a)+(b);
                int g = (100*a)+(10*c)+(b);
                int h = (100*b)+(10*a)+(c);
                int i = (100*c)+(10*b)+(a);
                if (d>e && d>f && d>g && d>h && d>i)
                    System.out.println("The greatest number formed is: " + d);
                else if (e>d && e>f && e>g && e>h && e>i)
                    System.out.println("The greatest number formed is: " + e);
                else if (f>d && f>e && f>g && f>h && f>i)
                    System.out.println("The greatest number formed is: " + f);
                else if (g>d && g>e && g>f && g>h && g>i)
                    System.out.println("The greatest number formed is: " + g);
                else if (h>d && h>e && h>f && h>g && h>i)
                    System.out.println("The greatest number formed is: " + h);
                else 
                    System.out.println("The greatest number formed is: " + i);
                if (d<e && d<f && d<g && d<h && d<i)
                    System.out.println("The smallest number formed is: " + d);
                else if (e<d && e<f && e<g && e<h && e<i)
                    System.out.println("The smallest number formed is: " + e); 
                else if (f<d && f<e && f<g && f<h && f<i)
                    System.out.println("The smallest number formed is: " + f);
                else if (g<d && g<e && g<f && g<h && g<i)
                    System.out.println("The smallest number formed is: " + g);
                else if (h<d && h<e && h<f && h<g && h<i)
                    System.out.println("The smallest number formed is:" + h);
                else
                    System.out.println("The smallest number formed is: " + i);
            }
	    else if((a==0)||(b==0)||(c==0))
	    {
	        if(a==0)
		{
	            if(b>c)
		    {
			System.out.println("The greatest number formed is: "+((b*100)+(c*10)+(a)));
		        System.out.println("The smallest number formed is: "+((c*10)+(b)));
		    }
		    else if(c>b)
	            {		    
			System.out.println("The greatest number formed is: "+((c*100)+(b*10)+(a)));
		        System.out.println("The smallest number formed is: "+((b*10)+(c)));
		    }
		}
		else if(b==0)
		{
	            if(a>c)
		    {	    
		        System.out.println("The greatest number formed is: "+((a*100)+(c*10)+(b)));
		        System.out.println("The smallest number formed is: "+((c*10)+(a)));
		    }
		    else if(c>a)
		    {
			System.out.println("The greatest number formed is: "+((c*100)+(a*10)+(b)));
		        System.out.println("The smallest number formed is: "+((a*10)+(c)));
		    }
		}
		else
		{
		    if(a>b)
		    {
		        System.out.println("The greatest number formed is: "+((a*100)+(b*10)+(c)));
			System.out.println("The smallest number formed is: "+((b*10)+(a)));
		    }
		    else if(b>a)
		    {
	                System.out.println("The greatest number formed is: "+((b*100)+(a*10)+(c)));
			System.out.println("The smallest number formed is: "+((a*10)+(b)));
		    }
		}
	    }
	    else if((a==0)&&(b==0)&&(c==0))
            {
	        System.out.println("The greatest number formed is 0.");
		System.out.println("The smallest number formed is 0.");
	    }
	    else	    
	    {
                System.out.println("Invalid input!");
	    }
	    System.out.println("Do you want to try again? (y or n): ");
	    x = sc.next().charAt(0);
	}
	while(x == 'y');
    }
}
