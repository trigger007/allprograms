import java.util.*;
public class pattern_3
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner(System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the number of terms required in the series: ");
            int num = sc.nextInt();
            for (int i=num ; i>=1 ; i--)
            {
                for (int k=1 ; k<=(num-i) ; k++)
                    System.out.print(" ");
                for (int j=1 ; j<=i ; j++)
                    System.out.print("! ");
                System.out.println();
            }
            System.out.println("Do you want to try again?(y or n)");
            x=sc.next().charAt(0);
        }
        while(x=='y');
    }
}
