import java.util.*;
public class choice
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
	char x = ' ';
	do
	{
            System.out.println("Enter your name: ");
            String name = sc.next();
            System.out.println("Enter the value of the laptop or desktop: ");
            int value = sc.nextInt();
            System.out.println("Enter 'l' for laptop and 'd' for desktop: ");
            char choice = sc.next().charAt(0);
            double disp;
            if(value>=1 && value<=25000)
                disp = choice == 'l' ? 0.0 : choice == 'd' ? 5.0 : 0.0;
            else if(value>=25001 && value<=57000)
                disp = choice == 'l' ? 5.0 : choice == 'd' ? 7.5 : 0.0;
            else if(value>=57001 && value<=100000)
                disp = choice == 'l' ? 7.5 : choice == 'd' ? 10.0 : 0.0;
            else
                disp = choice == 'l' ? 10.0 : choice == 'd' ? 15.0 : 0.0;
            double discount = (disp/100)*value;
            double NetAmount = value - discount;
            System.out.println("NAME: "+name);
            System.out.println("Discount offfered: Rs"+discount);
            System.out.println("PRICE TO BE PAID: "+NetAmount);
	    System.out.println();
	    System.out.println("do you want to try again? (y or n): ");
	    x = sc.next().charAt(0);
        }
	while(x == 'y');
    }
}
