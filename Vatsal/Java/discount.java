import java.util.*;
public class discount
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner(System.in);
	char x = ' ';
	do
	{
            System.out.println("Enter the cost of the laptop: ");
            long cost = sc.nextLong();
            double dvalue = 0;
            double tvalue = 0;
            if (cost <= 20000)
            {
                dvalue = (cost*5)/100;
                tvalue = cost-dvalue;
    	        System.out.println("The discount offered is: Rs"+dvalue);
                System.out.println("The total amount to be paid is: Rs" + tvalue);
            }
            else if (cost >= 20001 && cost <= 40000)
            {
                dvalue = (cost*15)/100;
		tvalue = cost-dvalue;
	        System.out.println("The discount offered is: Rs"+dvalue);
                System.out.println("The total amount to be paid is: Rs" + tvalue);
            }
            else if (cost >= 40001 && cost <= 50000)
            {
                dvalue = (cost*18)/100;
		tvalue = cost-dvalue;
	        System.out.println("The discount offered is: Rs"+dvalue);
                System.out.println("The total amount to be paid is: Rs" + tvalue);
            }
            else if (cost >= 50001)
            {
                dvalue = (cost*20)/100;
		tvalue = cost-dvalue;
	        System.out.println("The discount offered is: Rs"+dvalue);
                System.out.println("The total amount to be paid is: Rs" + tvalue);
            }
            else 
            {
                System.out.println("INVALID INPUT!!!");
            } 
	    System.out.println("Do you want to try again? (y or n): ");
	    x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
