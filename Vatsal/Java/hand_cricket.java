import java.util.*;
public class hand_cricket
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner (System.in);
        char x = ' ';
        System.out.println("This is the game of hand cricket!");
        System.out.println("You will be playing against the computer!");
        System.out.println("You will only be allowed to input numbers from 1 to 6");
        System.out.println("Lets start the play!");
        do
        {
            System.out.println("Enter 'B' for Balling OR Enter 'b' for batting: ");
            char b = sc.next().charAt(0);
            if(b == 'B')
            {
                System.out.println("You shall ball against the computer!");
		System.out.println("Try to get the computer out as fast as possible!");
                System.out.println("Start inputing your numbers (1 to 6)!");
                int ball=0 ;int m=0 ;int sum=0 ;
                do
                {
                    ball = sc.nextInt();
                    if(ball>6)
                    {
                        System.out.println("Invalid input! You are only allowed to input values ranging from 1 to 6");
                    }
                    else
                    {
                        m = (int)((Math.random()*6)+1);
                        System.out.println(m);
                        if(ball != m)
                            sum=sum+m;
                    }
                }
                while(ball != m);
                System.out.println("You have got the computer out!");
                System.out.println("Your target is "+(sum+1));
                System.out.println("It's your turn to bat now!");
                System.out.println("Hit the target score and you win!");
                System.out.println("Start inputing your numbers (1 to 6)!");
                int bat=0 ;int c=0 ;int add=0;
                do
                {
                    bat = sc.nextInt();
                    if(bat>6)
                    {
                        System.out.println("invalid input! You are only allowed to input values ranging from 1 to 6.");
                    }
                    else
                    {
                        c = (int)((Math.random()*6)+1);
                        System.out.println(c);
                        if(bat != c)
                             add=add+bat;
                    }
                }
                while(bat != c);
                System.out.println("The computer took down your wicket!");
                System.out.println("Your total score is "+add);
                if(add > sum)
                {
                    System.out.println("Your score is more than the computer's!");
                    System.out.println("YOU WIN !!!");
                }
                else if(sum > add)
                {
                    System.out.println("The computer's score is more than your score!");
                    System.out.println("YOU LOSE !!!");
                }
                else
                {
                    System.out.println("Your's and the computer's score is equal!");
                    System.out.println("IT'S A TIE !!!");
                }
            }
            else if(b =='b')
            {
                System.out.println("You shall bat against the computer and try to score as many runs as possible!");
                System.out.println("Start inputing your numbers (1 to 6)!");
                int ba=0 ;int co=0 ;int su=0;
                do
                {
                    ba = sc.nextInt();
                    if(ba>6)
                    {
                        System.out.println("Invalid input! You are only allowed to input values ranging from 1 to 6.");
                    }
                    else
                    {
                        co = (int)((Math.random()*6)+1);
                        System.out.println(co);
                        if(ba != co)
                            su=su+ba;
                    }
                }
                while(ba != co);
                System.out.println("The computer got you out!");
                System.out.println("Your total score is "+su);
                System.out.println("The target for the computer is "+(su+1));
		System.out.println("It's now your turn to ball!");
                System.out.println("You shall now try to get the computer out!");
                System.out.println("Start inputing your numbers (1 to 6)!");
                int bal=0 ;int comp=0 ;int ad=0;
                do
                {
                    bal = sc.nextInt();
                    if(bal>6)
                    {
                        System.out.println("Invalid input! You are only allowed to input values ranging from 1 to 6.");
                    }
                    else 
                    {
                        comp = (int)((Math.random()*6)+1);
                        System.out.println(comp);
                        if(bal != comp)
                            ad=ad+comp;
                    }
                }
                while(bal != comp);
                System.out.println("You took down the computer's wicket!");
                System.out.println("The computer's score is "+ad);
                if(ad>su)
                {
                    System.out.println("The computer's score is more than yours!");
                    System.out.println("YOU LOSE !!!");
                }
                else if(su>ad)
                {
                    System.out.println("Your score is more than the computer's!");
                    System.out.println("YOU WIN !!!");
                }
                else
                {
                    System.out.println("Your score and the computer's score is the same!");
                    System.out.println("IT'S A TIE !!!");
                }
            }
            else
            {
                System.out.println("Invalid input! You can only input 'B' for Balling OR 'b' for batting.");
            }
            System.out.println("Do you want to try again? (y or n): ");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
