import java.util.*;
public class neon
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        System.out.println("This program finds if a number is a neon number or not.");
        System.out.println("A neon number is a number whose sum of the square of the number is equal to the entered number.");
        char x = ' ';
        int b = 0;
        do
        {
            System.out.println("Enter a number: ");
            int num = sc.nextInt();
            int n = num * num;
            int a = 0;
            while (n>0)
            {
                a = n % 10;
                b = b + a;
                n = n / 10;
            }
            if ( b == num)
                System.out.println("The number is a neon number!");
            else 
                System.out.println("The number is not a neon number!");
            System.out.println("Do you want to try again? (y or n)");
            x = sc.next().charAt(0);
            b = 0;
        }
        while (x == 'y');
    }
}

    
