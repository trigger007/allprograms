import java.util.*;
public class pattern_5
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		char x = ' ';
		do
		{
		        System.out.println("Enter the number of rows required in the program: ");
		        int rows = sc.nextInt();
		        int num = 2;
		        for(int i=1 ; i<=rows ; i++)
		        {
			        for(int k=1 ; k<=(rows-i) ; k++)
		        	        System.out.print(" ");
			        for(int j=1 ; j<=i ; j++)
		                {
                                        if(num == 2)
				        {
					        System.out.print("1 ");
					        num--;
                                        }
				        else
				        {
					        System.out.print("2 ");
				        	num++;
				        }
			        }
			        System.out.println();
		        }
			System.out.println("Do you want to try again? (y or n): ");
			x=sc.next().charAt(0);
		}
		while(x == 'y');
	}
}
