import java.util.*;
public class reverse
{
	public static void main(String srgs[])
	{
		Scanner sc = new Scanner (System.in);
		char x = ' ';
		do 
		{
		        System.out.println("This program reverses the digits of any entered number");
		        System.out.println("Enter a number: ");
			int num = sc.nextInt();
			int d ;int rev=0 ;int n=num;
			while(num > 0)
			{
                                d = num%10;
			        rev = (rev*10)+d;
				num = num/10;
			}
			System.out.println("The reverse of the digit "+n+" is: "+rev);
			System.out.println("Do you want to try again? (y or n): ");
			x = sc.next().charAt(0);
		}
		while(x == 'y');
	}
}
