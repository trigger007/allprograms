import sys
sys.path.append('../packages')
import factorial

fact = 0
num = int(input("Enter a number: "))
if num < 0:
   print("Can only enter a positive number!")
   sys.exit(1)
fact=factorial.factorial(num)
print("The factorial of ",num," is ",fact)